const http = require('http');
const fs = require('fs');
const url = require('url');

module.exports = () => {
  function checkLog(res, fileName, method) {
    if (!fs.existsSync('./logs.json')) {
      fs.writeFileSync('./logs.json', '', err => {
        // if (err) throw err;
        if (err) {
          res.writeHead(400, {'Content-type': 'text/html'});
          console.log(`Could't create 'logs.json'`);
          res.end(`Could't create 'logs.json'`);
          return;
        }
      });
    }

    fs.readFile('./logs.json', (err, data) => {
      const logs = [];
      if (err) {
        res.writeHead(400, {'Content-type': 'text/html'});
        res.end(`"logs.json" hasn't found...`);
        return;
      } else {
        const obj = data.toString() !== '' ? JSON.parse(data) : {"logs" : logs}
        const operation = method === 'POST' ? 'created' : 'read';

        obj.logs.push({
          'message': `file with name '${fileName}' was ${operation}`,
          'time': Date.now()
        });

        fs.writeFile('./logs.json', `${JSON.stringify(obj, null, 2)}`, (err, data) => {
          // if (err) throw err;
          if (err) {
            res.writeHead(400, {'Content-type': 'text/html'});
            console.log(`Could't wrute in 'logs.json'`);
            res.end(`Could't write in 'logs.json'`);
            return;
          }
          
          res.writeHead(200, {'Content-type': 'application/json'});
          res.end(JSON.stringify(data));
        });
      }
    });
  }

  const server = http.createServer((req, res) => {
    if (req.url === '/favicon.ico') {
      res.writeHead(200, {'Content-Type': 'image/x-icon'} );
      res.end();
      return;
    }

    const {pathname, query} = url.parse(req.url, true);
    if (req.method === 'POST') {

      if (!query.content || query.content.trim() === '' || !query.filename || query.filename.trim() === '') {
        res.writeHead(400, {'Content-type': 'text/html'});
        console.log(`Content and filename should be valid`);
        res.end();
        return;
      }

      if (!fs.existsSync('./file')) {
        fs.mkdirSync('./file');
      }

      fs.writeFile(`./file/${query.filename}`, `${query.content}`, (err, data) => {
        if (err) {
          res.writeHead(400, {'Content-type': 'text/html'});
          console.log(`Could't make file with name '${query.filename}'`);
          res.end(`Could't make file with name '${query.filename}'`);
          return;
        }

        checkLog(res, query.filename, 'POST');

        res.writeHead(200, {'Content-type': 'text/html'});
        res.end(data);
      });

    } else {
      if (pathname === '/logs') {
        fs.readFile('./logs.json', 'utf-8', (err, content) => {
          if (err) {
            res.writeHead(400, {'Content-type': 'text/html'});
            res.end(`File with name 'logs.json' hasn't found...`);
            return;
          }

          if (Object.keys(query).length !== 0) {
            const logsArr = JSON.parse(content).logs;
            const from = +query.from;
            const to = +query.to;

            const logsArrFilter = logsArr.filter(item => item.time >= from && item.time <= to);
            const logsObjFilter = {"logs": logsArrFilter};

            res.writeHead(200, {'Content-Type': 'application/json'} );
            res.end(res.end(`${JSON.stringify(logsObjFilter, null, 2)}`));
            return;
          }

          res.writeHead(200, {'Content-Type': 'application/json'} );
          res.end(`${JSON.stringify(JSON.parse(content), null, 2)}`);
        });

      } else {

        fs.readFile(`./${pathname}`, (err, content) => {
          if (err) {
            res.writeHead(400, {'Content-type': 'text/html'});
            res.end(`File with name '${pathname.slice(6)}' hasn't found...`);
            return;
          }

          checkLog(res, pathname.slice(6), 'GET');

          res.writeHead(200, {'Content-type': 'text/html'});
          res.end(content);
        });
      }
    }
  });

  server.listen(process.env.PORT || 8080, () => console.log('The server is running on port 8080...'));
};
